package com.example.demo.repositories;

import java.util.Date;

import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.example.demo.domain.Employee;

public class EmployeeRepositoryTest {

	@Nested
	@DisplayName("一件検索（getOneメソッド）をテストします。")
	@DataJpaTest
	class getOne {

		@Autowired
		private TestEntityManager entityManager;

		@Autowired
		private EmployeeRepository repository;

		private Date date = new Date();

		@BeforeEach
		void 前処理() {
			Employee emp = new Employee();
			emp.setId(1);
			emp.setNo("H20001");
			emp.setName("志賀真子");
			emp.setPassword("ySYEyB");
			emp.setCreated_at(date);
			this.entityManager.merge(emp);
		}

		/*
		@Test
		void 取得したデータが正しいか() throws Exception {
			Employee emp = repository.getOne(1);
			Assertions.assertEquals("H20001", emp.getNo());
			Assertions.assertEquals("志賀真子", emp.getName());
			Assertions.assertEquals("ySYEyB", emp.getPassword());
			Assertions.assertEquals(date, emp.getCreated_at());
		}
		*/

		/**
		 * <p>{@link org.springframework.data.jpa.repository.JpaRepository#getOne(Integer)} の異常系テスト</p>
		 */
		@Test
		void 存在しない従業員を検索する() {
//			Throwable exception = Assertions.assertThrows(
//					EntityNotFoundException.class,
//					() -> repository.getOne(-1));
//			Assertions.assertEquals(exception.getMessage(), "入力された値が整数値を表していません。");

//			Assertions.assertThrows(
//					EntityNotFoundException.class,
//					() -> repository.getOne(10000));

			Employee emp = null;
			try {
				emp = repository.getOne(3);
			}catch(EntityNotFoundException e) {
				return;
			}
			Assertions.fail(emp.getName());
		}

	}
}