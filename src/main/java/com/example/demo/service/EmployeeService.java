package com.example.demo.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.domain.Employee;
import com.example.demo.domain.Employee.EmployeeBuilder;
import com.example.demo.form.EmployeeInputForm;
import com.example.demo.form.IndexForm;
import com.example.demo.repositories.EmployeeRepository;
import com.example.demo.utils.Message;
import com.example.demo.utils.TestUtil;

@Service
@Transactional
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	/**
	 * 引数の従業員番号と一致する従業員を返す。
	 * @param no 従業員番号
	 * @return {@code not null} Employee 検索の結果見つかった従業員エンティティ
	 */
	public Employee findByNo(String no) throws EntityNotFoundException {
		return employeeRepository.findByNo(no);
	}

	/**
	 * 引数のIDと一致する従業員を返す。
	 * @param id 従業員ID
	 * @return {@code not null} Employee 検索の結果見つかった従業員エンティティ
	 */
	public Employee findById(Integer id) throws EntityNotFoundException {
		return employeeRepository.getOne(id);
	}

	/**
	 * 引数の従業員番号と部分一致する従業員を返す。
	 * @param no 引数の従業員番号と部分一致する従業員を返す。
	 * @return {@code not null} Employee 検索の結果見つかった従業員エンティティのリスト
	 */
	public List<Employee> findByFuzzyNo(String no) throws EntityNotFoundException {
		return employeeRepository.findFuzzyByNo(no);
	}

	/**
	 * 引数の名前と部分一致する従業員を返す。
	 * @param name 検索を行う名前
	 * @return {@code not null} Employee 検索の結果見つかった従業員エンティティのリスト
	 */
	public List<Employee> findByFuzzyName(String name) throws EntityNotFoundException {
		return employeeRepository.findFuzzyByName(name);
	}

	/**
	 * 複数の従業員番号を指定して検索を行う。<br>
	 * <br>
	 * 複数の従業員番号を含む文字列を引数として取り、Specificationを使いor句として解釈できるようなSQLを構築する。<br>
	 * 組み立てたSpecificationは最終的にfindAllによって実行され、結果を返す。
	 * @param numberQuery 複数の従業員番号を含む文字列
	 * @return List<Employee> 検索結果
	 */
	public List<Employee> findByNumbers(String numberQuery) throws EntityNotFoundException {

		// 何もしないSpecificationを生成する。reduceの初期値として利用する
		// Specification.where()にnullを渡せば、何もしないSpecificationが生成される
		final Specification<Employee> zero = Specification.where((Specification<Employee>) null);

		// クエリを複数キーワードに分割し、それぞれSpecificationにマッピングして、orで結合する
		final Specification<Employee> spec = splitQuery(numberQuery)
				.stream()
				.map(this::numberContains)
				.reduce(zero, Specification<Employee>::or);

		return employeeRepository.findAll(spec);
	}

	/**
	 * 複数の従業員名を指定して検索を行う。<br>
	 * <br>
	 * 複数の従業員名を含む文字列を引数として取り、Specificationを使いor句として解釈できるようなSQLを構築する。<br>
	 * 組み立てたSpecificationは最終的にfindAllによって実行され、結果を返す。
	 * @param nameQuery 複数の従業員名を含む文字列
	 * @return List<Employee> 検索結果
	 */
	public List<Employee> findByNames(String nameQuery) throws EntityNotFoundException {

		// 何もしないSpecificationを生成する。reduceの初期値として利用する
		// Specification.where()にnullを渡せば、何もしないSpecificationが生成される
		final Specification<Employee> zero = Specification.where((Specification<Employee>) null);

		// クエリを複数キーワードに分割し、それぞれSpecificationにマッピングして、orで結合する
		final Specification<Employee> spec = splitQuery(nameQuery)
				.stream()
				.map(this::nameContains)
				.reduce(zero, Specification<Employee>::or);

		return employeeRepository.findAll(spec);
	}

	/**
	 * 論理削除されていないすべてのレコードを検索する。
	 */
	public ModelAndView index(
			ModelAndView mv,
			ModelMap modelMap)
			throws EntityNotFoundException {

		List<Employee> employees = employeeRepository.findAllNotSoftDeleted();

		mv.addObject("indexForm", new IndexForm());
		mv.addObject("employees", employees);
		mv.addObject("message", (Message) modelMap.get("message"));

		mv.setViewName("employees/index");
		return mv;
	}

	/**
	 * 論理削除されているレコードを検索する。
	 */
	public ModelAndView softdeletedIndex(
			ModelAndView mv,
			ModelMap modelMap)
			throws EntityNotFoundException {

		List<Employee> employees = employeeRepository.findAllSoftDeleted();
		mv.addObject("indexForm", new IndexForm());
		mv.addObject("employees", employees);
		mv.addObject("softDelete", true);
		mv.addObject("message", (Message) modelMap.get("message"));

		mv.setViewName("employees/index");
		return mv;
	}

	public ModelAndView search(
			ModelAndView mv,
			RedirectAttributes redirectAttributes,
			String searchValue,
			String searchTargetColumn) throws EntityNotFoundException {

		List<Employee> employees = null;

		// プルダウンの値に応じて、実行させるサービスを分岐させる
		switch (searchTargetColumn) {
		case "no":
			employees = findByNumbers(searchValue);
			break;
		case "name":
			employees = findByNames(searchValue);
			break;
		default:
			employees = employeeRepository.findAll();
		}
		mv.addObject("employees", employees);

		// 検索を行い、該当する行が見つかった場合、一覧画面を返す
		if (!CollectionUtils.isEmpty(employees)) {
			mv.addObject("indexForm", new IndexForm()); // 従業員登録用フォーム
			mv.setViewName("employees/index");
			return mv;
		}

		mv.addObject("searchValue", searchValue);
		mv.addObject("searchTargetColumn", searchTargetColumn);

		// リダイレクト先に送るメッセージオブジェクトの生成
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("message", new Message("検索結果が見つかりませんでした。", "failed"));
		redirectAttributes.addFlashAttribute("model", modelMap);

		mv.setViewName("redirect:/employees");
		return mv;
	}

	/**
	 * 全件検索を行う処理です。
	 */
	public List<Employee> findAll() throws EntityNotFoundException {
		return employeeRepository.findAll();
	}

	/**
	 * 新しい従業員を登録する。
	 */
	public ModelAndView create(
			ModelAndView mv,
			EmployeeInputForm form,
			BindingResult result,
			RedirectAttributes redirectAttributes)
			throws EntityNotFoundException {

		if (result.hasErrors()) {
			// エラーが発生した場合、元の画面に戻る
			mv.setViewName("employees/input");
			return mv;
		}

		Employee employee = new Employee().toBuilder()
				.no("")
				.name(form.getName())
				.password(form.getPassword())
				.created_at(new Date())
				.build();

		Employee saveEmployee = employeeRepository.save(employee);
		saveEmployee.setNo(String.format("H2%04d", saveEmployee.getId()));
		employeeRepository.save(saveEmployee);

		// リダイレクト先に送るメッセージオブジェクトの生成
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("message", new Message("登録が完了しました。", "success"));
		redirectAttributes.addFlashAttribute("model", modelMap);

		mv.setViewName("redirect:/employees");
		return mv;
	}

	/**
	 * 変更対象となる従業員情報を取得し、ModelAndViewにくっつける。
	 */
	public ModelAndView getUpdateTarget(
			ModelAndView mv,
			IndexForm form)
			throws EntityNotFoundException {

		// 変更対象の従業員を検索し、modelに付加する
		Employee employee = employeeRepository.getOne(form.getTarget());
		System.out.println(employee);

		if (Objects.isNull(employee)) {
			throw new EntityNotFoundException();
		}

		mv.addObject("employee", employee);
		mv.addObject("employeeInputForm", new EmployeeInputForm()); // 従業員登録用フォーム

		mv.setViewName("/employees/update");
		return mv;
	}

	/**
	 * 引数として渡された従業員オブジェクトのパラメータを変更する。
	 */
	public ModelAndView update(
			ModelAndView mv,
			EmployeeInputForm form,
			BindingResult result,
			RedirectAttributes redirectAttributes)
			throws EntityNotFoundException {

		Integer id = form.getId();

		if (!employeeRepository.existsById(id)) {
			throw new EntityNotFoundException("変更対象の従業員が見つかりませんでした。");
		}

		Employee employee = employeeRepository.getOne(id);

		if (result.hasErrors()) {
			// 変更情報入力画面に戻る
			mv.addObject("employee", employee);
			mv.setViewName("employees/update");
			return mv;
		}

		// clone を実装するのが面倒だったので、lombokのBuilderを使って浅いコピーを行う
		Employee newEmployee = employee.toBuilder()
				.name(form.getName())
				.password(form.getPassword())
				.build();

		ModelMap modelMap = new ModelMap();

		System.out.println(employee);
		System.out.println(newEmployee);

		System.out.println(employee.equals(newEmployee));

		// 値の変更に有無があったかをcheckする
		if (employee.equals(newEmployee)) {
			modelMap.addAttribute("message", new Message("各フォームの値が同一だったため、変更処理を行いませんでした。", "warn"));
			redirectAttributes.addFlashAttribute("model", modelMap);
			mv.setViewName("redirect:/employees");
			return mv;
		}

		employee.setUpdated_at(new Date());
		employee = employeeRepository.save(employee);

		// リダイレクト先に送るメッセージオブジェクトの生成
		modelMap.addAttribute("message", new Message("変更が完了しました。", "success"));
		redirectAttributes.addFlashAttribute("model", modelMap);

		mv.setViewName("redirect:/employees");
		return mv;
	}

	/**
	 * IDと一致する従業員を論理削除する。
	 */
	public ModelAndView softDelete(
			ModelAndView mv,
			IndexForm form,
			RedirectAttributes redirectAttributes)
			throws EntityNotFoundException {

		Integer id = form.getTarget();

		//		Employee employee = employeeRepository.getOne(id);
		//		if (Objects.isNull(employee)) {
		//			throw new EntityNotFoundException();
		//		}

		if (!employeeRepository.existsById(id)) {
			throw new EntityNotFoundException("削除対象の従業員が見つかりませんでした。");
		}
		int recordNum = employeeRepository.softDeleteById(id);

		// リダイレクト先に送るメッセージオブジェクトの生成
		Message message = new Message("論理削除が完了しました。", "success");
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("message", message);
		redirectAttributes.addFlashAttribute("model", modelMap);

		mv.setViewName("redirect:/employees");
		return mv;
	}

	/**
	 * IDと一致する従業員を物理削除する。
	 */
	public ModelAndView delete(
			ModelAndView mv,
			IndexForm form,
			RedirectAttributes redirectAttributes)
			throws EntityNotFoundException {

		Integer id = form.getTarget();
		if (!employeeRepository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		employeeRepository.deleteById(id);

		// リダイレクト先に送るメッセージオブジェクトの生成
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("message", new Message("物理削除が完了しました。", "success"));
		redirectAttributes.addFlashAttribute("model", modelMap);

		mv.setViewName("redirect:/employees/deleted");
		return mv;
	}

	/**
	 * 削除済みのレコードを復元する。
	 */
	public ModelAndView restore(
			ModelAndView mv,
			IndexForm form,
			RedirectAttributes redirectAttributes)
			throws EntityNotFoundException {

		Integer id = form.getTarget();
		int recordNum = employeeRepository.restore(id);
		if (recordNum == 0) {
			throw new EntityNotFoundException("復元対象の従業員が見つかりませんでした。");
		}

		// リダイレクト先に送るメッセージオブジェクトの生成
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("message", new Message("復元が完了しました。", "success"));
		redirectAttributes.addFlashAttribute("model", modelMap);

		mv.setViewName("redirect:/employees/deleted");
		return mv;
	}

	/**
	 * 従業員テーブルのレコード全件を物理削除する。
	 * @param なし
	 * @return なし
	 */
	public void deleteAll() throws EntityNotFoundException {
		employeeRepository.deleteAll();
	}

	/**
	 * 引数に渡されたクエリ文字列を、カンマ（,|、）、スペース（ |　）でパースし、文字列型のListに格納して返す。
	 * @param query パース対象とする検索文字列
	 * @return List<String>
	 */
	private List<String> splitQuery(String query) {
		final String space = " ";
		// 半角/全角スペースとカンマの組み合わせのパターンを表す
		final String spacesPattern = "[\\s　,、]+";
		// 以上のパターンにマッチした部分を単一の半角スペースに変換する
		final String monoSpaceQuery = query.replaceAll(spacesPattern, space);
		// splitするとき、余分な空要素が生成されるのを防ぐため、先頭と末尾のスペースを削除する
		final String trimmedMonoSpaceQuery = monoSpaceQuery.trim();
		// 半角スペースでクエリをsplitする
		return Arrays.asList(trimmedMonoSpaceQuery.split("\\s"));
	}

	/**
	 * 引数として渡した複数の従業員番号を含む文字列を、like句として解釈できる形に変換する
	 * @param no 複数の従業員番号を含む文字列
	 */
	private Specification<Employee> numberContains(String no) {
		return new Specification<Employee>() {
			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.like(root.get("no"), "%" + no + "%");
			}
		};
	}

	/**
	 * 引数として渡した複数の従業員名を含む文字列を、like句として解釈できる形に変換する
	 *  @param name 複数の従業員名を含む文字列
	 */
	private Specification<Employee> nameContains(String name) {
		return new Specification<Employee>() {
			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.like(root.get("name"), "%" + name + "%");
			}
		};
	}

	public void test() throws EntityNotFoundException {
		//		TestUtil.throwEntityNotFoundException();
//		System.out.println(employeeRepository.isDeleted(1));

		int[] idArray = {1, 58, 2, 91};
		for(int i: idArray) {
//			int num = employeeRepository.isDeleted(i);
//			System.out.println(String.format("%d", num));
			boolean b= isDeleted(i);
			System.out.println(String.format("%b", b));
		}
	}

	private boolean isDeleted(Integer id) {
		int recordNum = employeeRepository.isDeleted(id);
		if(recordNum == 1) {
			return true;
		}else {
			return false;
		}
	}
}
