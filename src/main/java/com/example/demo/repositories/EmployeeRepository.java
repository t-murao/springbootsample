package com.example.demo.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.domain.Employee;

/**
 * @author 村尾 知樹
 */
@Repository
@Transactional
public interface EmployeeRepository extends JpaRepository<Employee, Integer>, JpaSpecificationExecutor<Employee> {

	/**
	 * 引数に渡した文字列を従業員番号に含む従業員を検索し、複数のレコードを返します。<br>
	 * このメソッドは、曖昧な検索（カラムに引数の文字列を含んでいるかの判定）を行います。
	 * @param no 従業員番号
	 * @return List<Employee> 検索の結果見つかったEmployeeを要素として持つList
	 * @author 村尾 知樹
	 */
	@Query("SELECT x FROM Employee x WHERE x.no LIKE %:no% ORDER BY x.no")
	public List<Employee> findFuzzyByNo(@Param("no") String no) throws EntityNotFoundException;

	/**
	 * 引数に渡した文字列を名前に含む従業員を検索し、複数のレコードを返します。<br>
	 * このメソッドは、曖昧な検索（カラムに引数の文字列を含んでいるかの判定）を行います。
	 * @param name 従業員名
	 * @return Employee 従業員EntityのList
	 * @author 村尾 知樹
	 */
	@Query("SELECT x FROM Employee x WHERE x.name LIKE %:name% ORDER BY x.name")
	public List<Employee> findFuzzyByName(@Param("name") String name) throws EntityNotFoundException;

	/**
	 * 引数に渡した従業員番号と一致する従業員を検索し、単一のレコードを返します。
	 * @param no 従業員番号
	 * @return List<Employee> 検索の結果見つかったEmployeeを要素として持つList
	 * @author 村尾 知樹
	 */
	@Query("SELECT x FROM Employee x WHERE x.no = :no")
	public Employee findByNo(@Param("no") String no) throws EntityNotFoundException;

	/**
	 * 引数で指定したIDの従業員を論理削除します。
	 * @param id 論理削除したい従業員ID（文字列）
	 * @return なし
	 * @author 村尾 知樹
	 */
	@Transactional
	@Modifying
	@Query("UPDATE Employee x SET x.updated_at = NOW(), x.deleted_at = NOW() WHERE id = :id")
	public int softDeleteById(@Param("id") Integer id) throws EntityNotFoundException;

	/**
	 * 論理削除されていないレコードを検索し、複数レコードを返します。
	 * @param なし
	 * @return List<Employee>
	 * @author 村尾 知樹
	 */
	@Query("SELECT x FROM Employee x WHERE x.deleted_at IS NULL")
	public List<Employee> findAllNotSoftDeleted();

	/**
	 * 論理削除されてたレコードを検索し、複数レコードを返します。
	 * @param なし
	 * @return {@linkplain Employee 顧客}のリスト
	 * @author 村尾 知樹
	 */
	@Query("SELECT x FROM Employee x WHERE x.deleted_at IS NOT NULL")
	public List<Employee> findAllSoftDeleted();

	/**
	 * この従業員の論理削除を解除します。
	 * @param id 復元したい従業員の従業員ID
	 * @return 論理削除を解除した件数（既に論理削除済みの従業員を指定しても{@code 1}を返します）
	 * @author 村尾 知樹
	 * @throws EntityNotFoundException 存在しない従業員IDが指定された場合にスローされる
	 */
	@Transactional
	@Modifying
	@Query("UPDATE Employee x SET x.updated_at = NOW(), x.deleted_at = NULL WHERE x.id = :id AND x.deleted_at IS NOT NULL")
	public int restore(@Param("id") Integer id) throws EntityNotFoundException;

	/**
	 * <p>この従業員が論理削除されているかを検査します。</p>
	 * @param id 検査したい従業員の従業員ID
	 * @return 削除済みであれば {@code 1}、論理削除されていなければ {@code 0} を返します
	 */
//	@Query("SELECT count(x) FROM Employee x WHERE x.id = :id AND x.deleted_at IS NOT NULL")
	@Query("SELECT CASE WHEN (x.deleted_at IS NOT NULL) THEN true ELSE false END FROM Employee x WHERE x.id = :id AND x.deleted_at IS NOT NULL")
	public int isDeleted(@Param("id") Integer id);
}
