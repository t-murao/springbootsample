use jsysdb;

DROP TABLE IF EXISTS customers;

-- /*================================================================*/
-- /*  従業員（employees）テーブル作成                                */
-- /*================================================================*/

CREATE TABLE employees (
	id INTEGER(4) NOT NULL AUTO_INCREMENT,
	no VARCHAR(6) NOT NULL,
	name VARCHAR(32) NOT NULL,
	password VARCHAR(6) NOT NULL,
	created_at DATETIME DEFAULT NULL,
	updated_at DATETIME DEFAULT NULL,
	deleted_at DATETIME DEFAULT NULL,
	PRIMARY KEY(id)
)engine=InnoDB;